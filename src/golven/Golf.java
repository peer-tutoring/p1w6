package golven;

public class Golf {
    private double amplitude;
    private double frequentie;

    public Golf() {
        this.amplitude = 1.0;
        this.frequentie = 1.0;
    }

    public void setAmplitude(double amplitude) {
        if (frequentie > 0){
            this.amplitude = amplitude;
        }
    }

    public void setFrequentie(double frequentie) {
        if (frequentie > 0){
            this.frequentie = frequentie;
        }
    }

    public double getYwaarde(double x){
        //y = amplitude * sin sin (frequentie * x)

        return amplitude * Math.sin(frequentie * x);
    }

    @Override
    public String toString() {
        if (amplitude == 1){
            return String.format("y = sin (%.1f x)", frequentie);
        }
        else if(frequentie == 1){
            return String.format("y = %.1f sin x", amplitude);
        }
        else if(frequentie == 1 && amplitude == 1){
            return String.format("y = sin x");
        }
        else{
            return String.format("y = %.1f sin (%.1f x)", amplitude, frequentie);
        }

    }
}
