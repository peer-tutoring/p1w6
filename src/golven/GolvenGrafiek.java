package golven;

import java.util.Random;
import java.awt.*;

public class GolvenGrafiek {
    Random rnd;
    int aantal;

    public GolvenGrafiek(int aantal) {
        this.aantal = aantal;
        rnd = new Random();
    }

    public void tekenGolven(){
        GrafiekWindow grafiekWindow = new GrafiekWindow(10,6);

        for (int i = 0; i <= aantal; i++){
            Golf golf = new Golf();

            double amp = rnd.nextDouble() * 4.0;
            double freq = rnd.nextDouble()  * 4.0;
            golf.setAmplitude(amp);
            golf.setFrequentie(freq);

            Color color = new Color(rnd.nextInt(254) + 1, rnd.nextInt(254) + 1, rnd.nextInt(254) + 1);

            for (double j = -5.0; j < 5.0; j+=0.0001){
                grafiekWindow.tekenPunt(j, golf.getYwaarde(j), color);
            }
        }

        grafiekWindow.toon();
    }
}
